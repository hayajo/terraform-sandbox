# S3とCDN(CloudFlare)によるホームページホスティング

* オリジンをS3としてCDN(CloudFlare)でコンテンツを配信する構成
* 事前準備が必要
    - ドメイン登録
    - CloudFlareゾーン登録
    - ネームサーバ変更


WordPress + StaticPress(VagrantやDockerで実行環境構築)で作成した静的コンテンツをS3にアップロードしてCDNで配信したり。

## 構成

![homepage_s3_cdn](img/homepage_s3_cdn.png)


## 事前準備

* ドメイン(example.com)登録
    - 任意のレジストラ(お名前,com、freenom、etc.)
    - DNS設定
        - example.comのAもしくはCNAMEレコード
            - ダミーとして適当なコンテンツを返すホストを指定（CloudFlareのゾーン登録時にチェックされるため）

* CloudFlareにゾーン登録(example.com)
    - ゾーン登録後にレジストラのネームサーバをCloudFlareのものに変更する
    - レコードはTerraformで追加するので未登録でも可
        - Terraformでは`$(var.subdomain}.${var.domain}`(e.g. blog.example.com)のレコードのみを登録する
        - ルートドメイン("@")のAやMXレコード、その他サブドメイン(wwwなど)のレコードは適宜手作業で登録する


## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=backend.tfvars
$ terraform plan
...
```

