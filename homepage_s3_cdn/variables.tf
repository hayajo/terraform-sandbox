# AWS Credentials
variable "aws" {
  default = {
    access_key = ""
    secret_key = ""
    region     = "ap-northeast-1"
  }
}

# Cloudflare
variable "cloudflare" {
  default = {
    email = ""
    token = ""
  }
}

# Service Name
variable "service_name" {
  default = "myhomepage"
}

variable "domain" {} # e.g. example.com

variable "subdomain" {
  default = "blog" # e.g. blog => blog.${var.domain} => blog.example.com
}
