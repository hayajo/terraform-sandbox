provider "cloudflare" {
  email = "${var.cloudflare["email"]}"
  token = "${var.cloudflare["token"]}"
}

resource "cloudflare_record" "main" {
  domain  = "${var.domain}"
  name    = "${var.subdomain}"
  type    = "CNAME"
  value   = "${aws_s3_bucket.main.website_endpoint}"
  proxied = true
  ttl     = 120
}

output "domain" {
  value = "${length(var.subdomain) != 0 ? join(".", list(var.subdomain, var.domain)) : var.domain}"
}

output "hostname" {
  value = "${cloudflare_record.main.hostname}"
}
