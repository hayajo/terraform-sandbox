# Credentials
provider "aws" {
  version = "~> 0.1"

  access_key = "${var.aws["access_key"]}"
  secret_key = "${var.aws["secret_key"]}"
  region     = "${var.aws["region"]}"
}

resource "aws_s3_bucket" "main" {
  bucket        = "${length(var.subdomain) != 0 ? join(".", list(var.subdomain, var.domain)) : var.domain}"
  acl           = "public-read"
  force_destroy = true
  policy        = "${data.aws_iam_policy_document.s3.json}"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  versioning {
    enabled = true
  }

  tags {
    Name  = "${var.service_name}"
    Stage = "${terraform.workspace}"
  }
}

resource "aws_s3_bucket_object" "index" {
  bucket       = "${aws_s3_bucket.main.bucket}"
  key          = "index.html"
  source       = "html/index.html"
  etag         = "${md5(file("html/index.html"))}"
  content_type = "text/html; charset=utf-8"
}

resource "aws_s3_bucket_object" "error" {
  bucket       = "${aws_s3_bucket.main.bucket}"
  key          = "error.html"
  source       = "html/error.html"
  etag         = "${md5(file("html/error.html"))}"
  content_type = "text/html; charset=utf-8"
}

output "s3 website endpoint" {
  value = "${aws_s3_bucket.main.website_endpoint}"
}


data "aws_iam_policy_document" "s3" {
  statement {
    actions = [
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::${length(var.subdomain) != 0 ? join(".", list(var.subdomain, var.domain)) : var.domain}/*",
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
  }
}
