variable "hosts" {
  default = [
    "host01",
    "host02",
    "host03",
  ]
}

data "template_file" "list_other_items" {
  count = "${length(var.hosts)}"
  template = "${var.hosts[count.index]} => $${hosts}"
  vars {
    hosts = "${join(", ", concat(slice(var.hosts, 0, count.index), slice(var.hosts, count.index + 1, length(var.hosts))))}"
  }
}

output "result" {
  value = "${data.template_file.list_other_items.*.rendered}"
}

