# AWS Credentials
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}

# Service Name
variable "service_name" {
  default = "myapp"
}

# Available Zone
variable "az" {
  default = ["a", "c"]
}

# AMI
variable "amis" {
  default = {
    ecs = "ami-e4657283" # see. http://docs.aws.amazon.com/ja_jp/AmazonECS/latest/developerguide/ecs-optimized_AMI.html
  }
}

# Instance Type
variable "instance_type" {
  default = {
    container_instance = "t2.micro"
  }
}

# Container
variable "containers" {
  default = ["frontend", "backend"]
}

# CloudWatch Log Group
# variable "cloudwatch_log_group" {
  # default = "${var.service_name}-${terraform.workspace}"
# }

# Plublic Key(for ECS Container Instance)
variable "public_key" {}
