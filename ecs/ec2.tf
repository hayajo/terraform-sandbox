# Key Pair
resource "aws_key_pair" "main" {
  key_name   = "${var.service_name}-${terraform.workspace}"
  public_key = "${var.public_key}"
}

# Launch Configuration
resource "aws_launch_configuration" "ecs_instance" {
  name_prefix = "${var.service_name}-${terraform.workspace}-"

  image_id                    = "${var.amis["ecs"]}"
  instance_type               = "t2.micro"
  iam_instance_profile        = "${aws_iam_instance_profile.ecs.id}"
  key_name                    = "${aws_key_pair.main.key_name}"
  associate_public_ip_address = true # Public IP or NAT or HTTP Proxy are necessary to access the AWS ECS service endpoint.

  user_data = "${data.template_file.ecs_instance.rendered}"

  security_groups = [
    "${aws_default_security_group.main.id}",
    "${aws_security_group.ssh.id}",
  ]

  lifecycle {
    create_before_destroy = true
  }
}

data "template_file" "ecs_instance" {
  template = "${file("provision/ecs.tpl")}"

  vars {
    ecs_cluster = "${aws_ecs_cluster.main.name}"
  }
}

# Auto Scaling Group
resource "aws_autoscaling_group" "ecs_cluster" {
  name = "${var.service_name}-${terraform.workspace}-ecs_cluster"

  vpc_zone_identifier  = ["${aws_subnet.public.*.id}"]
  launch_configuration = "${aws_launch_configuration.ecs_instance.name}"

  min_size         = 1
  max_size         = 2
  desired_capacity = 2

  force_delete = true

  tags = [
    {
        key                 = "Name"
        value               = "${var.service_name}-${terraform.workspace}-ecs_instance"
        propagate_at_launch = true
    }
  ]

  lifecycle {
    create_before_destroy = true
  }
}
