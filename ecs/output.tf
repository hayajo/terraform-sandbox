output "DOCKER_LOGIN_COMMAND" {
  value = "eval $(aws ecr get-login --no-include-email --region=${var.region})"
}

output "REPOSITORY_URL" {
  value = "${aws_ecr_repository.main.*.repository_url}"
}

output "ELB DNS" {
  value = "${aws_elb.frontend.dns_name}"
}
