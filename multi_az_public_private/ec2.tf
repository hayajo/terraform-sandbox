# Key Pair
resource "aws_key_pair" "default" {
  key_name = "${var.service_name}-${terraform.workspace}"
  public_key = "${var.public_key}"
}

# EC2(bastion)
resource "aws_instance" "bastion" {
  count = "${length(var.az)}"
  ami = "${var.amis["ap-northeast-1"]}"
  instance_type = "${var.instance_type["bastion"]}"
  key_name = "${aws_key_pair.default.key_name}"
  vpc_security_group_ids = [
    "${aws_default_security_group.default.id}",
    "${aws_security_group.bastion.id}"
  ]
  subnet_id = "${element(aws_subnet.public.*.id, count.index % length(var.az))}"
  associate_public_ip_address = true
  tags {
    Name = "${var.service_name}-${terraform.workspace}-bastion${format("%02d", (count.index / length(var.az)) + 1)}${var.az[count.index % length(var.az)]}"
  }
}

# EIP(nat)
resource "aws_eip" "nat" {
  count = "${length(var.az)}"
  vpc = true
}

# EIP(bastion)
resource "aws_eip" "bastion" {
  count = "${length(var.az)}"
  vpc = true
}

resource "aws_eip_association" "bastion" {
  count = "${length(var.az)}"
  instance_id = "${element(aws_instance.bastion.*.id, count.index)}"
  allocation_id = "${element(aws_eip.bastion.*.id, count.index)}"
}
