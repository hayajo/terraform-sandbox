terraform {
  version = "~> 0.10.0"
  backend "s3" {}
}

# Variables
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}

# AWS Provider
provider "aws" {
  version = "~> 0.1"

  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region = "${var.region}"
}

## VPC
resource "aws_vpc" "zabbix" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_vpc" "production" {
  cidr_block = "10.1.0.0/16"
}

resource "aws_vpc" "development" {
  cidr_block = "10.2.0.0/16"
}

## VPC Peering
resource "aws_vpc_peering_connection" "production" {
  vpc_id      = "${aws_vpc.zabbix.id}"
  peer_vpc_id = "${aws_vpc.production.id}"
  auto_accept = true
}

resource "aws_vpc_peering_connection" "development" {
  vpc_id      = "${aws_vpc.zabbix.id}"
  peer_vpc_id = "${aws_vpc.development.id}"
  auto_accept = true
}

## Route Table
resource "aws_route_table" "zabbix" {
  vpc_id = "${aws_vpc.zabbix.id}"
  route {
    cidr_block = "10.1.0.0/16"
    gateway_id = "${aws_vpc_peering_connection.production.id}"
  }
  route {
    cidr_block = "10.2.0.0/16"
    gateway_id = "${aws_vpc_peering_connection.development.id}"
  }
}

resource "aws_route_table" "production" {
  vpc_id = "${aws_vpc.production.id}"
  route {
    cidr_block = "10.0.0.0/16"
    gateway_id = "${aws_vpc_peering_connection.production.id}"
  }
}

resource "aws_route_table" "development" {
  vpc_id = "${aws_vpc.development.id}"
  route {
    cidr_block = "10.0.0.0/16"
    gateway_id = "${aws_vpc_peering_connection.development.id}"
  }
}
