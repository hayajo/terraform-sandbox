# VPC Peering

* 異なるVPC間を接続する
* zabbix環境とproduction環境、development環境を接続


## 構成

![vpc_peering](img/vpc_peering.png)

## tfstateファイル

このプロジェクトではterraform 0.9以降の[backend](https://www.terraform.io/docs/backends/index.html)
を利用し、`main.tf`でtfstateファイルををS3に保存するように定義しています。

次のような内容の`tfvars`ファイルを作成し`terraform init`を実行してから利用して
ください。

```
bucket="mybucket"
key="path/to/tfstate"
region="ap-northeast-1"
access_key="access key"
secret_key="secret key"
```

```
$ terraform init -backend-config=backend.tfvars
$ terraform plan
...
```

