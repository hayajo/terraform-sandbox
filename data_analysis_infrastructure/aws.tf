# Credentials
provider "aws" {
  version = "~> 0.1"

  access_key = "${var.aws["access_key"]}"
  secret_key = "${var.aws["secret_key"]}"
  region     = "${var.aws["region"]}"
}
