# Credentials
provider "google" {
  version = "~> 0.1"

  credentials = "${file(var.gcp["credentials"])}"
  project     = "${var.gcp["project"]}"
  region      = "${var.gcp["region"]}"
}
