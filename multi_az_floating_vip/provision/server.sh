#!/bin/sh

REGION=ap-northeast-1
ROLE=server
VIP=192.168.1.1

INSTANCE_ID=$(curl -s 169.254.169.254/latest/meta-data/instance-id)
MAC_ETH0=$(curl -s 169.254.169.254/latest/meta-data/mac)
VPC_ID=$(curl -s 169.254.169.254/latest/meta-data/network/interfaces/macs/$MAC_ETH0/vpc-id)
UNICAST_PEER=$(aws --region=$REGION ec2 describe-instances --filter "Name=tag-key,Values=Role" "Name=tag-value,Values=$ROLE" "Name=network-interface.vpc-id,Values=$VPC_ID" --query "Reservations[].Instances[?InstanceId!=\`$INSTANCE_ID\`].PrivateIpAddress[]" --output=text | sed -e 's/\t\+/\n/g')


### install packages
yum install -y keepalived nginx


### generate configs

# keepalived.conf
cat <<EOF >/etc/keepalived/keepalived.conf
! Configuration File for keepalived

global_defs {
   router_id LVS_DEVEL
}

vrrp_instance VI_1 {
    state BACKUP
    nopreempt
    interface eth0
    virtual_router_id 51
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    unicast_peer {
        $UNICAST_PEER
    }
    virtual_ipaddress {
        $VIP
    }
    notify_master "/etc/keepalived/notify_master.sh $REGION $VIP"
}
EOF

chmod 0644 /etc/keepalived/keepalived.conf

# notify_master.sh
cat <<'EOF' >/etc/keepalived/notify_master.sh
#!/bin/sh

REGION=$1
VIP=$2

INSTANCE_ID=$(curl -s 169.254.169.254/latest/meta-data/instance-id)
SUBNET_ID=$(aws --region=$REGION ec2 describe-instances --filter "Name=instance-id,Values=$INSTANCE_ID" --query "Reservations[].Instances[].SubnetId" --output=text)
ROUTE_TABLE_ID=$(aws --region=$REGION ec2 describe-route-tables --filter "Name=association.subnet-id,Values=$SUBNET_ID" --query "RouteTables[].RouteTableId" --output=text)

aws --region=$REGION ec2 delete-route --destination-cidr-block=$VIP/32 --route-table-id=$ROUTE_TABLE_ID
aws --region=$REGION ec2 create-route --destination-cidr-block=$VIP/32 --route-table-id=$ROUTE_TABLE_ID --instance-id=$INSTANCE_ID
EOF

chmod 0744 /etc/keepalived/notify_master.sh


### enable and start servers

# nginx
chkconfig nginx on
service nginx start

# keepalives
chkconfig keepalived on
service keepalived start

