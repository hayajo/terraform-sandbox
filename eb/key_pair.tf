# Key Pair
resource "aws_key_pair" "main" {
  key_name   = "${var.service_name}-${terraform.workspace}"
  public_key = "${var.public_key}"
}
