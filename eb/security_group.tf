resource "aws_default_security_group" "main" {
  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port = 0
    to_port   = 0
    protocol  = -1
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}

resource "aws_security_group" "ssh" {
  name        = "${var.service_name}-${terraform.workspace}-ssh"
  description = "${var.service_name}-${terraform.workspace}-ssh"

  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}-ssh"
  }
}

resource "aws_security_group" "myapp" {
  name        = "${var.service_name}-${terraform.workspace}-myapp"
  description = "${var.service_name}-${terraform.workspace}-myapp"

  vpc_id = "${aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.service_name}-${terraform.workspace}-myapp"
  }
}
