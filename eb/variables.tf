# AWS Credentials
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}

# Service Name
variable "service_name" {
  default = "eb-wordpress"
}

# "wordpress" docker-image tag
variable "docker_image_tag" {
  default = "latest"
}

# MySQL Engine Version
variable "mysql_engine_version" {
  default = "5.6.35"
}

# Database Storage(GB)
variable "db_storage" {
  default = 5
}

# Database Username
variable "db_username" {}

# Database Password
variable "db_password" {}

# Instance Type
variable "instance_type" {
  default = {
    instance = "t2.micro"
    db       = "db.t2.micro"
  }
}

# Availavitily Zone
variable "az" {
  default = ["a", "c"]
}

# Plublic Key
variable "public_key" {
  default = ""
}
