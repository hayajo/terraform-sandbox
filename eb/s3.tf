# S3 Bucket
resource "aws_s3_bucket" "main" {
  bucket = "${var.service_name}-${terraform.workspace}"
  acl           = "private"

  tags {
    Name = "${var.service_name}-${terraform.workspace}"
  }
}
