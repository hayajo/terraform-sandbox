variable "string" {
  default = "Hello World"
}

variable "list" {
  type = "list"
  default = ["a", "b", "c"]
}

variable "map" {
  type = "map"
  default = {
    key1 = "value1"
    key2 = "value2"
    key3 = "value3"
  }
}

data template_file "main" {
  template = "${file("template.txt")}"
  vars {
    string = "${var.string}"
    list = "${join(", ", var.list)}"
    map = "${join(", ", formatlist("%s=%s", keys(var.map), values(var.map)))}"
  }
}

output "main" {
  value = "${data.template_file.main.rendered}"
}
