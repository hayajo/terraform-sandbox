# AWS Credentials
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}

# Service Name
variable "service_name" {
  default = "multi_az_floating_eip"
}

# Available Zone
variable "az" {
  default = ["a", "c"]
}

# AMI
variable "amis" {
  default = {
    ap-northeast-1 = "ami-3bd3c45c"
  }
}

# Instance Type
variable "instance_type" {
  default = {
    bastion = "t2.nano"
    server = "t2.nano"
  }
}

# Plublic Key
variable "public_key" {}

# Private key path
variable "private_key_path" {}
