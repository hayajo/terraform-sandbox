#!/bin/sh

REGION=ap-northeast-1
ROLE=server

INSTANCE_ID=$(curl -s 169.254.169.254/latest/meta-data/instance-id)
MAC_ETH0=$(curl -s 169.254.169.254/latest/meta-data/mac)
VPC_ID=$(curl -s 169.254.169.254/latest/meta-data/network/interfaces/macs/$MAC_ETH0/vpc-id)
UNICAST_PEER=$(aws --region=$REGION ec2 describe-instances --filter "Name=tag-key,Values=Role" "Name=tag-value,Values=$ROLE" "Name=network-interface.vpc-id,Values=$VPC_ID" --query "Reservations[].Instances[?InstanceId!=\`$INSTANCE_ID\`].PrivateIpAddress[]" --output=text | sed -e 's/\t\+/\n/g')


### install packages
yum install -y keepalived nginx

### generate configs

# keepalived.conf
cat <<EOF >/etc/keepalived/keepalived.conf
! Configuration File for keepalived

global_defs {
   router_id LVS_DEVEL
}

vrrp_script check_web {
    script "/etc/keepalived/check_web.sh"
    interval 2
    fail 2
    raise 2
}

vrrp_instance VI_1 {
    state BACKUP
    nopreempt
    interface eth0
    virtual_router_id 51
    priority 100
    advert_int 1
    authentication {
        auth_type PASS
        auth_pass 1111
    }
    unicast_peer {
        $UNICAST_PEER
    }
    track_script {
        check_web
    }
    notify_master "/etc/keepalived/notify_master.sh $REGION"
}
EOF

chmod 0644 /etc/keepalived/keepalived.conf


# notify_master.sh
cat <<'EOF' >/etc/keepalived/notify_master.sh
#!/bin/sh

REGION=$1

PUBLIC_IP=${public_ip}

INSTANCE_ID=$(curl -s 169.254.169.254/latest/meta-data/instance-id)
ASSOCIATION_ID=$(aws --region=$REGION ec2 describe-addresses --query 'Addresses[].AssociationId' --output text --public-ips=$PUBLIC_IP)
ALLOCATION_ID=$(aws --region=$REGION ec2 describe-addresses --query 'Addresses[].AllocationId' --output text --public-ips=$PUBLIC_IP)

aws --region=$REGION ec2 disassociate-address --association-id=$ASSOCIATION_ID
aws --region=$REGION ec2 associate-address --allocation-id=$ALLOCATION_ID --instance=$INSTANCE_ID
EOF

chmod 0744 /etc/keepalived/notify_master.sh


# check_web.sh
cat <<'EOF' >/etc/keepalived/check_web.sh
#!/bin/sh
HOST="localhost"
timeout 3 curl 127.0.0.1 -H "Host: $HOST" >/dev/null 2>&1
EOF

chmod 0744 /etc/keepalived/check_web.sh


### enable and start servers

# nginx
chkconfig nginx on
service nginx start

sleep 5

# keepalives
chkconfig keepalived on
service keepalived start
