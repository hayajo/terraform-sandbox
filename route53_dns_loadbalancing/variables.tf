# AWS Credentials
variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "ap-northeast-1"
}

# Service Name
variable "service_name" {
  default = "route53-dns-failover"
}

# Availavitily Zone
variable "az" {
  default = ["a", "c"]
}

# Plublic Key
variable "public_key" {
  default = ""
}

variable "instance_count" {
  default = 2
}

variable "lightsail_blueprint" {
  # NOTE: aws lightsail --region=REGION get-blueprints
  default = "nginx_1_12_0_2"
}

variable "lightsail_bundle" {
  # NOTE: aws lightsail --region=REGION get-bundles
  default = "nano_1_0"
}

variable "domain" {}
