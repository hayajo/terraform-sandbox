output "IP Addresses of Lightsail instances" {
  value = "${aws_lightsail_static_ip.main.*.ip_address}"
}

output "Name Servers of Route53" {
  value = "${aws_route53_zone.main.name_servers}"
}

output "Domain" {
  value = "${var.domain}"
}
